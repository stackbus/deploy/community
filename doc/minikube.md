
[<-- Return to main README](../README.md)

# Getting started: Run a local minikube deployment

This section will guide you to locally run a local deployment recipe, although most other recipes on this project are to be run in the cloud.

## 1. Pre-requisites
* Docker installed. See https://www.docker.com/products/docker-desktop/
* Kubectl and Minicube installed 
See https://kubernetes.io/docs/tasks/tools/
* Minikube installed. 
See https://minikube.sigs.k8s.io/docs/start/

## 2. Verify docker installation
In your terminal type

    docker --version

You should see output similar to this

    Docker version 20.10.6, build 370c289

## 3. Start and Verify your Minikube 

In your terminal type
    
    % minikube start

You should see something like this

    minikube v1.31.2 ....
    ....a few more lines and info here...
    ....
    Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default

## 4. Verify your kubectl

In your terminal type

    kubectl version

you should see something like this

    Client Version: v1.28.3
    Kustomize Version: v5.0.4-0.20230601165947-6ce0bf390ce3
    Server Version: v1.27.4

## 5. Deploy with kubectl

In your terminal type

    kubectl apply -f '*.yaml' 

You can access at

    127.0.0.1:30080

if that does not work (likely if you are running the minikube from docker),
then open a terminal and type the command below and leave it running. 
    
    kubectl port-forward svc/stackbus-runtime 30080:3000

then you should be able to access it at

    127.0.0.1:30080

Then when done exit with Ctrl+c


if it still not running try the command below (and also leave running). 
    
    minikube service stackbus-runtime

this will output something like this and will open your browser

    |-----------|------------------|-------------|---------------------------|
    | NAMESPACE |       NAME       | TARGET PORT |            URL            |
    |-----------|------------------|-------------|---------------------------|
    | default   | stackbus-runtime | http/3000   | http://192.168.49.2:30080 |
    |-----------|------------------|-------------|---------------------------|
    🏃  Starting tunnel for service stackbus-runtime.
    |-----------|------------------|-------------|------------------------|
    | NAMESPACE |       NAME       | TARGET PORT |          URL           |
    |-----------|------------------|-------------|------------------------|
    | default   | stackbus-runtime |             | http://127.0.0.1:60620 |
    |-----------|------------------|-------------|------------------------|
    🎉  Opening service default/stackbus-runtime in default browser..

Then when done exit with Ctrl+c

