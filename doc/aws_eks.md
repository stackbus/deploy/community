[<-- Return to main README](../README.md)

# Deploying to Amazon EKS


What is Amazon EKS?

EKS stands for Elastic Kubernetes Service. It is Amazon's fully managed Kubernetes control panel. By using this service you don't have to setup and maintain your Kubernetes Master nodes, you just deploy your Kubernetes infrastructure and Amazon takes care of managing the Master node in a fail-safe manner (for the master node).

For more information in Amazon EKS visit
https://aws.amazon.com/eks/


## IMPORTANT: Make sure to Delete your cluster once you are done or you will be billed by Amazon

## Configuring AWS credentials to use with the AWS CLI

### 1. Create IAM User in AWS Web Console & download CSV
* Login to the AWS Web console with your root user or a user with permissions to create a new IAM user and IAM policies
* Create Policy "EksAllAccess" with the following JSON content:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "eks:*",
            "Resource": "*"
        },
        {
            "Action": [
                "ssm:GetParameter",
                "ssm:GetParameters"
            ],
            "Resource": [
                "arn:aws:ssm:*:<account_id>:parameter/aws/*",
                "arn:aws:ssm:*::parameter/aws/*"
            ],
            "Effect": "Allow"
        },
        {
            "Action": [
            "kms:CreateGrant",
            "kms:DescribeKey"
            ],
            "Resource": "*",
            "Effect": "Allow"
        },
        {
            "Action": [
            "logs:PutRetentionPolicy"
            ],
            "Resource": "*",
            "Effect": "Allow"
        }        
    ]
}
```


    Source: https://eksctl.io/usage/minimum-iam-policies/

* Create Policy "IamLimitedAccess" with the following JSON content:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "iam:CreateInstanceProfile",
                "iam:DeleteInstanceProfile",
                "iam:GetInstanceProfile",
                "iam:RemoveRoleFromInstanceProfile",
                "iam:GetRole",
                "iam:CreateRole",
                "iam:DeleteRole",
                "iam:AttachRolePolicy",
                "iam:PutRolePolicy",
                "iam:AddRoleToInstanceProfile",
                "iam:ListInstanceProfilesForRole",
                "iam:PassRole",
                "iam:DetachRolePolicy",
                "iam:DeleteRolePolicy",
                "iam:GetRolePolicy",
                "iam:GetOpenIDConnectProvider",
                "iam:CreateOpenIDConnectProvider",
                "iam:DeleteOpenIDConnectProvider",
                "iam:TagOpenIDConnectProvider",
                "iam:ListAttachedRolePolicies",
                "iam:TagRole",
                "iam:GetPolicy",
                "iam:CreatePolicy",
                "iam:DeletePolicy",
                "iam:ListPolicyVersions"
            ],
            "Resource": [
                "arn:aws:iam::<account_id>:instance-profile/eksctl-*",
                "arn:aws:iam::<account_id>:role/eksctl-*",
                "arn:aws:iam::<account_id>:policy/eksctl-*",
                "arn:aws:iam::<account_id>:oidc-provider/*",
                "arn:aws:iam::<account_id>:role/aws-service-role/eks-nodegroup.amazonaws.com/AWSServiceRoleForAmazonEKSNodegroup",
                "arn:aws:iam::<account_id>:role/eksctl-managed-*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "iam:GetRole"
            ],
            "Resource": [
                "arn:aws:iam::<account_id>:role/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "iam:CreateServiceLinkedRole"
            ],
            "Resource": "*",
            "Condition": {
                "StringEquals": {
                    "iam:AWSServiceName": [
                        "eks.amazonaws.com",
                        "eks-nodegroup.amazonaws.com",
                        "eks-fargate.amazonaws.com"
                    ]
                }
            }
        }
    ]
}
``` 
    Source: https://eksctl.io/usage/minimum-iam-policies/

* Create a new IAM group "stackbus" with the required permission policies (below) to run EKSCTL. 

    at the time of this writing these are:
    * AmazonEC2FullAccess
    * AWSCloudFormationFullAccess
    * EksAllAccess
    * IamLimitedAccess

    For up to date list of these permissions refer to
    * https://github.com/eksctl-io/eksctl
    * https://eksctl.io/usage/minimum-iam-policies/

* Create an IAM user "stackbus-user" and assign to group stackbus

* Generate access keys for user and download the CSV. DO NOT COMMIT TO SOURCE CONTROL, DO NOT SHARE. You may save it in the "tokens" folder of this project as that is in the .gitignore list and does not get committed. Inside this file you will have the following data which you will need to configure eksctl: 
    * Access key ID
    * Secret access key


Once you create the IAM user make sure to download the credencials.csv file and copy it to the tokens/ folder of this project. If none exists then create the folder. This project is configured not to commit such folder. If you place them elsewhere ensure to add them to the .gitignore file and in any case MAKE SURE NO TO COMMIT TO YOUR SOURCE CONTROL.

### 2. Build the docker image to run aws cli, eksctl and kubectl

    cd devenv
    ./build_awscli.sh

### 3. Configure the IAM user for use by the AWS CLI

    Enter the awscli image (you can later exit typing "exit")
    cd devenv
    ./awscli.sh

-

    Once inside the image prompt:
    aws configure

    Follow the interactive prompts and enter your:
    * Access key ID
    * Secret access key
    * region

    for region use a valid AWS region. Example: us-east-1

### 4. Verify the IAM user configuration

Still inside the awscli image, run the command 
    aws configure list

    You should see something like this:

    bash-4.2# aws configure list
    Name                    Value             Type    Location
    ----                    -----             ----    --------
    profile                <not set>             None    None
    access_key     ****************53ID shared-credentials-file    
    secret_key     ****************wq0N shared-credentials-file    
    region                us-east-1      config-file    ~/.aws/config



### 5. Configure EKS

Still inside the awscli image, run the command: kubectl version

    aws eks --region us-east-1 update-kubeconfig --name stackbus

-

    The above should create this file in your project (do not delete, do not commit)
         tokens/.kube/config

### 6. Verify kubectl is working

Still inside the awscli image, run the command: kubectl version

    You should see something like this:
    bash-4.2# kubectl version
    Client Version: v1.28.4
    Kustomize Version: v5.0.4-0.20230601165947-6ce0bf390ce3
    Server Version: v1.27.7-eks-4f4795d

### 7. Create a cluster in AWS (this takes like 20 minutes)

Still inside the awscli image, run the following command to create a cluster with 2 nodes (you may specify 1 if you prefer): 

    eksctl create cluster --name stackbus --nodes-min=2 

### 8. Describe your cluster
Still inside the awscli image, run the following command

    kubectl get all

    You should see something like:

    NAME                TYPE       CLUSTER-IP  EXTERNAL-IP  PORT(S) AGE
    service/kubernetes  ClusterIP  10.100.0.1    <none>  

### 9. Deploy the aws_minimal recipe
Still inside the awscli image,

    cd aws_minimal
    kubectl apply -f workloads.yaml
    kubectl apply -f services.yaml

### 10. Verify deployment
Still inside the awscli image,

    kubectl get all

You should see something similar to this
``` 
NAME                         READY   STATUS    RESTARTS   AGE
pod/runtime-64f99466-6d5x4   1/1     Running   0          51m

NAME                       TYPE           CLUSTER-IP       EXTERNAL-IP                                                               PORT(S)          AGE
service/kubernetes         ClusterIP      10.100.0.1       <none>                                                                    443/TCP          82m
service/stackbus-runtime   LoadBalancer   10.100.155.146   a9ab6cb5d25fa4b9d9b790843165d8cc-1722177388.us-east-1.elb.amazonaws.com   3000:30080/TCP   51m

NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/runtime   1/1     1            1           51m

NAME                               DESIRED   CURRENT   READY   AGE
replicaset.apps/runtime-64f99466   1         1         1       51m
``` 

### 11. Open your browser in the address of the LoadBalancer in port 3000
From my example above that is:
```
    a9ab6cb5d25fa4b9d9b790843165d8cc-1722177388.us-east-1.elb.amazonaws.com:3000
```
You should see the basic page "Hello World Again!"

### 12. Delete your Cluster (this takes like 7 minutes, WAIT UNTIL IT IS DONE)
Still inside the awscli image,

    eksctl delete cluster stackbus

I recommend anyhow logging into the AWS web console and visit the EC2 instances, load balancers and security groups just to ensure they all got deleted (give it some time and refresh the browser a few times). If you still see the created resources then delete manually (should not be the case unless the delete command had some failure or was terminated before it finished)

## Resources

EKSCTL - The official CLI of Amazon EKS
https://github.com/eksctl-io/eksctl

## Troubleshooting

### Finding the Kubernetes version of Amazon EKS

    * login to aws web console
    * go to eks
    * press button to create cluster (you will not actually create it)
    * on next page see drop down for kubernetes version and see which one is the default version, at time of writing this it is 1.28

### Unable to import credentials CSV fila 
The CSV file for the IAM user is likely missing the User Name column and also it is likely in non-UTF8 encoding, and the eksctl tool does not like that. That is the reason in this tutorial I use "eksctl configure" interactively

    see https://github.com/aws/aws-cli/issues/7721

### Problems connecting from kubectl to AWM Cluster

See https://repost.aws/knowledge-center/eks-cluster-connection

### qemu: uncaught target signal 11 (Segmentation fault)

I got this problem when I was using kubectl installing in docker image manually, but once I changed the dockerfile to use yum then this problem went away.

If you are running on a Mac M1 inside the built image awscli bash and you get the following error while running kubectl commands:

    bash-4.2# kubectl get all
    qemu: uncaught target signal 11 (Segmentation fault) - core dumped
    Segmentation fault

Then you need to first install/update quemu on your Mac. To do so follow these steps:


or with macports:
https://www.macports.org/install.php
https://ports.macports.org/port/qemu/

1. install xcode command line tools (needed to install Brew, skip if already installed)

    MacOS> xcode-select --install

2. Download and install Brew (skip if already installed)

    https://github.com/Homebrew/brew/releases
    
    e.g.
    https://github.com/Homebrew/brew/releases/download/4.1.20/Homebrew-4.1.20.pkg

3. Install quemu
    
    MacOS> brew install qemu

4. Verify quemu
    
    MacOS> qemu-system-arm -version
    QEMU emulator version 5.1.0
    Copyright (c) 2003-2020 Fabrice Bellard and the QEMU Project developers

Sources:
* https://worldaiiotcongress.org/wp-content/uploads/2022/05/Part-1-MacOS-Install-QEMU.pdf
* https://www.qemu.org/download/#macos