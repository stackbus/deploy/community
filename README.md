# Stackbus Community Recipes


This project contains all the Official Deployment Recipes for Stackbus Community Edition. More info below.

## Running a local deployment

To run a local deployment using minikube follow the instructions here:

[Getting started: Run a local minikube deployment](doc/minikube.md)

[Deploy to AWS](doc/aws_eks.md)

## Special folders
* devenv Contains

## Deployment Recipe Folders
Each non-special folder in this project contains one deployment recipe.

A deployment recipe consists of 
* One descriptor file named: stackbus_deployment.json
* One or more deployment files. 

The format of the deployment files depends on the deployment type, that is, which technology is used for the deployment. For Kubernetes deployment type that would be one or more YAML (*.yaml) files 

## Deployment Recipe Descriptor files

These files describe metadata for a given recipe. This metadata will be used by the Stackbus UI to display available recipes and by the Stackbus Deployment Engine to know how to execute a given recipe.

Sample descriptor file: stackbus_deply.json

    {
        "spec-version":"1.0",
        "id":"minikube_minimal",
        "version":"1.0.0",
        "version-description":"Initial implementation",
        "deployment-type":"kubernetes",
        "target-system":"local",
        "target-host":"minikube",
        "label":"Minikube Minimal Deployment Recipe",
        "description":"This recipe performs a minimal local deployment into Minikube",
        "author": "Daniel Castañeda <daniel@kwantec.com>"
    }




## Getting started

## Deployment types

### Kubernetes



