#!/bin/bash
set -e

echo ''
echo ''
echo 'eksctl.sh  USAGE. '
echo '' 
echo '   This script is used to run eksctl commands'
echo '   The container instance from where the commands'
echo '   are run has the root folder of this project (../)'
echo '   mapped to the /aws folder (you need to cd into it first)'           
echo '' 
echo '   Usage ./eksctl.sh [EKSCTL_COMMAND]'
echo ''        
echo '         EKSCTL_COMMAND   Optional, if specified it runs the provided'
echo '                          eksctl command and then exits'
echo '                          If not provided it enters the eksctl'
echo '                          instance sh console.' 
echo ''

mkdir -p $(pwd)/../tokens/.aws
if [ -z "${1}" ]
  then
    docker run --rm -it -v $(pwd)/../tokens/.aws:/root/.aws -v $(pwd)/..:/aws --entrypoint /bin/sh public.ecr.aws/eksctl/eksctl
  else
    docker run --rm -it -v $(pwd)/../tokens/.aws:/root/.aws -v $(pwd)/..:/aws public.ecr.aws/eksctl/eksctl ${1}
fi
