#!/bin/bash
set -e


echo ''
echo ''
echo 'awscli.sh'
echo '' 
echo '   NOTE: For this script to work '
echo '         YOU MUST FIRST BUILD THE IMAGE'
echo '         by running once the build_awscli.sh script '
echo ''USAGE 
echo '   This script is used to run AWS CLI and EKSCTL commands'
echo '   The container instance from where the commands'
echo '   are run has the root folder of this project (../)'
echo '   mapped to the /aws folder and enters is already cd there'
echo '   when you enter the instance console'           
echo '' 
echo '   Usage ./awscli.sh ["AWS_CLI_COMMAND"]'
echo ''        
echo '         AWS_CLI_COMMAND  Optional, '
echo '                          YOU MUST WRAP IN DOUBLE QUOTES'
echo '                          so that all ARGs are passed to the image',
echo '                          if specified it runs the provided '
echo '                          AWS CLI command and then exits.'
echo '                          If not provided it enters the AWS CLI'
echo '                          instance bash console.' 
echo ''

mkdir -p $(pwd)/../tokens/.aws
if [ -z "${1}" ]
  then
    docker run --rm -it -v $(pwd)/../tokens/.aws:/root/.aws -v $(pwd)/..:/aws --entrypoint /bin/bash --name awscli2 aws-cli
  else
    docker run --rm -it -v $(pwd)/../tokens/.aws:/root/.aws -v $(pwd)/..:/aws --name awscli2 aws-cli ${1}
fi
