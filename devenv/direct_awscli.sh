#!/bin/bash
set -e


echo ''
echo ''
echo 'awscli.sh  USAGE. '
echo '' 
echo '   This script is used to run eksctl commands'
echo '   The container instance from where the commands'
echo '   are run has the root folder of this project (../)'
echo '   mapped to the /aws folder. It is already the workdir'
echo '   when you enter the instance console'           
echo '' 
echo '   Usage ./awscli.sh [AWS_CLI_COMMAND]'
echo ''        
echo '         AWS_CLI_COMMAND   Optional, if specified it runs the provided'
echo '                          AWS CLI command and then exits'
echo '                          If not provided it enters the AWS CLI'
echo '                          instance bash console.' 
echo ''

mkdir -p $(pwd)/../tokens/.aws
if [ -z "${1}" ]
  then
    docker run --rm -it -v $(pwd)/../tokens/.aws:/root/.aws -v $(pwd)/..:/aws --entrypoint /bin/bash --name awscli public.ecr.aws/aws-cli/aws-cli
  else
    docker run --rm -it -v $(pwd)/../tokens/.aws:/root/.aws -v $(pwd)/..:/aws --name awscli public.ecr.aws/aws-cli/aws-cli ${1}
fi
